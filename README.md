## Installation

### S root právy

	sudo apt install git python-pip # nebo python3-pip
	sudo pip install ansible #nebo pip3

### Bez root práv

	pip install --user ansible

### Examples
	git clone https://gitlab.labs.nic.cz/mvician/ansible-introduction-examples
	cd ansible-introduction-examples

## Run

Spusť soubor `??-run.sh`, který odpovídá příkazům ze slajdů, takže je nemusíš přepisovat!

## Ansible Tower

https://www.youtube.com/watch?v=ToXoDdUOzj8
