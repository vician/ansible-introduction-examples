#!/bin/bash

which ansible-inventory-grapher 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "Please install ansible-inventory-grapher, e.g.: pip(3) install ansible-inventory-grapher"
	exit 1
fi

which dot 1>/dev/null 2>/dev/null
if [ $? -ne 0 ]; then
	echo "Please install graphviz"
	exit 1
fi

ansible-inventory-grapher -q -i 07-hosts all | dot -Tpng | display png:-
