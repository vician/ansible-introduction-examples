#!/bin/bash
set -x
ansible-playbook -i 11-hosts playbooks/11-sudo.yml
echo "----------------------------------------------"
ansible-playbook -i 11-hosts playbooks/11-sudo.yml --become --ask-become-pass
